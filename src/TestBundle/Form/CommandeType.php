<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommandeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('order_id', 'text', array('label'=>'label.order.id', 'translation_domain' => 'TestBundle', 'attr'=>array('placeholder'=>'placeholder.order.id')))
            ->add('marketplace', 'integer', array('label'=>'label.marketplace', 'translation_domain' => 'TestBundle', 'attr'=>array('placeholder'=>'placeholder.marketplace')))
            ->add('order_mrid', 'text', array('label'=>'label.order.mrid', 'translation_domain' => 'TestBundle', 'attr'=>array('placeholder'=>'placeholder.order.mrid')))
            ->add('order_refid', 'text', array('label'=>'label.order.refid', 'translation_domain' => 'TestBundle', 'attr'=>array('placeholder'=>'placeholder.order.refid')))
            ->add('order_external_id', 'text', array('label'=>'label.order.external.id', 'translation_domain' => 'TestBundle', 'attr'=>array('placeholder'=>'placeholder.order.external.id')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Commande'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'testbundle_commande';
    }
}
