<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use TestBundle\Entity\Commande;
use TestBundle\Form\CommandeType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Response;

class CommandeController extends Controller
{
    /**
     *
     * @Route("/load/",name="loadXML_commande", methods={"GET"})
     */
    public function loadXMLAction()
    {
        $fluxXML = $this->container->get('lengow_test');
        $fluxXML->load();
        $fluxXML->toDataBase($this->getDoctrine()->getManager());

        return $this->redirectToRoute('admin_commandes');
    }


    /**
     * @return
     * @Route("/admin",name="admin_commandes", methods={"GET","POST"})
     */
    public function AdminAction()
    {
        $grid = $this->get('grid');

        $source = new Entity('TestBundle:Commande');

        $grid->setSource($source);

        return $grid->getGridResponse('TestBundle:Commande:gridCommande.html.twig');
    }

    /**
     * @param $id_order
     * @param $format
     * @return $formatedResponse
     * @Route("/api/",name="commande_json", methods={"GET"})
     * @Route("/api/{format}",name="commande_yaml", methods={"GET"}, requirements={"format":"yaml"})
     * @Route("/api/{id_order}",name="one_commande_json", methods={"GET"})
     */
    public function JsonAction($id_order=null, $format=null)
    {
        if ($id_order) {
            $commandes = $this->getDoctrine()->getManager()->getRepository('TestBundle\Entity\Commande')->find($id_order);
        } else {
            $commandes = $this->getDoctrine()->getManager()->getRepository('TestBundle\Entity\Commande')->findAll();
        }

        if ($format) {
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers);

            $yamlCommande = Yaml::dump(array('commandes'=>$serializer->normalize($commandes)), 6);

            $yamlResponse = new Response();
            $yamlResponse->setContent($yamlCommande);
            $yamlResponse->headers->set('Content-type', 'yaml');
            $yamlResponse->sendHeaders();
            $formatedResponse = $yamlResponse;
        } else {
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers);

            $normalizeCommande = array('commandes'=>$serializer->normalize($commandes));

            $formatedResponse = new JsonResponse($normalizeCommande);
        }

        return $formatedResponse;
    }

    /**
     * Add commande
     * @param $request
     * @Route("/commande/add",name="add_commande",methods={"POST","GET"})
     */
    public function addAction(Request $request)
    {
        $commande = new Commande();

        $commandeForm = $this->createForm(new CommandeType(), $commande);

        if ($request->isMethod('POST') && $commandeForm->handleRequest($request)) {
            if ($this->getDoctrine()->getManager()->getRepository('TestBundle:Commande')->find($commande->getOrderId())) {
                $request->getSession()->getFlashBag()->add('notice', 'order already exist');
                
                return $this->redirectToRoute('add_commande');
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($commande);
                $em->flush();

                return $this->redirectToRoute('admin_commandes');
            }
        }

        return $this->render('TestBundle:Commande:add.html.twig', array('commandeForm' => $commandeForm->createView()));
    }
}
