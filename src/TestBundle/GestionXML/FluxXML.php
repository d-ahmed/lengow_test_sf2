<?php

namespace TestBundle\GestionXML;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use TestBundle\Entity\Commande;

class FluxXML
{
    /**
     * @var $url
     */
    private $url;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var $xml
     */
    private $xml;

    /**
     * FluxXML constructor.
     * @param $url
     * @param LoggerInterface $logger
     */
    public function __construct($url, LoggerInterface $logger)
    {
        $this->url = $url;
        $this->logger = $logger;
    }

    /**
     * Load
     */
    public function load()
    {
        try {
            $this->xml = new \SimpleXMLElement($this->url, null, true);
            $this->logger->debug('load_xml', array('url'=>$this->url, 'response'=>$this->xml));
        } catch (\Exception $e) {
            $this->logger->error('load_xml_error', array('url'=>$this->url, 'message'=>$e->getMessage()));
        }
    }

    /**
     * @param EntityManager $manager
     */
    public function toDataBase(EntityManager $manager)
    {
        if ($this->xml) {
            foreach ($this->xml->orders->order as $order) {
                $entityOrder = new Commande();
                $entityOrder->setOrderId($order->order_id)
                            ->setMarketplace($order->marketplace)
                            ->setOrderPurchaseDate(new \DateTime($order->order_purchase_date))
                            ->setOrderPurchaseHeure(new \DateTime($order->order_purchase_heure))
                            ->setOrderMrid($order->order_mrid)
                            ->setOrderRefid($order->order_refid)
                            ->setOrderExternalId($order->order_external_id);

                $manager->merge($entityOrder);
            }
            $manager->flush();
        }
    }
}
