<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="TestBundle\Entity\CommandeRepository")
 * @ORM\Table(name="test_order")
 *
 */

class Commande
{

    /**
     * @var $order_id
     * @ORM\Column(type="string", length=255)
     * @ORM\Id
     * @GRID\Column(field="order_id", title="label.order.id", translation_domain="TestBundle")
     */
    private $order_id;

    /**
     * @var $marketplace
     * @ORM\Column(type="integer", nullable=true)
     * @GRID\Column(field="marketplace", title="label.marketplace", translation_domain="TestBundle")
     */
    private $marketplace;

    /**
     * @var $order_mrid
     * @ORM\Column(type="string",  nullable=true)
     * @GRID\Column(field="order_mrid", title="label.order.mrid", translation_domain="TestBundle")
     */
    private $order_mrid;

    /**
     * @var $order_refid
     * @ORM\Column(type="string", nullable=true)
     * @GRID\Column(field="order_refid", title="label.order.refid", translation_domain="TestBundle")
     */
    private $order_refid;

    /**
     * @var $order_external_id
     * @ORM\Column(type="string", nullable=true)
     * @GRID\Column(field="order_external_id", title="label.order.external.id", translation_domain="TestBundle")
     */
    private $order_external_id;

    /**
     * @var $order_purchase_date
     * @ORM\Column(type="date", nullable=true)
     * @GRID\Column(field="order_purchase_date", title="label.order.purchase.date", translation_domain="TestBundle")
     */
    private $order_purchase_date;

    /**
     * @var $order_purchase_heure
     * @ORM\Column(type="time", nullable=true)
     * @GRID\Column(field="order_purchase_heure", title="label.order.purchase.heure", translation_domain="TestBundle")
     */
    private $order_purchase_heure;

    /**
     * Commande constructor.
     */
    public function __construct()
    {
        $this->setOrderPurchaseDate(new \DateTime());
        $this->setOrderPurchaseHeure(new \DateTime());
    }

    

    /**
     * Set order_id
     *
     * @param string $orderId
     * @return Commande
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * Get order_id
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set marketplace
     *
     * @param integer $marketplace
     * @return Commande
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return integer
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set order_mrid
     *
     * @param string $orderMrid
     * @return Commande
     */
    public function setOrderMrid($orderMrid)
    {
        $this->order_mrid = $orderMrid;

        return $this;
    }

    /**
     * Get order_mrid
     *
     * @return string
     */
    public function getOrderMrid()
    {
        return $this->order_mrid;
    }

    /**
     * Set order_refid
     *
     * @param string $orderRefid
     * @return Commande
     */
    public function setOrderRefid($orderRefid)
    {
        $this->order_refid = $orderRefid;

        return $this;
    }

    /**
     * Get order_refid
     *
     * @return string
     */
    public function getOrderRefid()
    {
        return $this->order_refid;
    }

    /**
     * Set order_external_id
     *
     * @param string $orderExternalId
     * @return Commande
     */
    public function setOrderExternalId($orderExternalId)
    {
        $this->order_external_id = $orderExternalId;

        return $this;
    }

    /**
     * Get order_external_id
     *
     * @return string
     */
    public function getOrderExternalId()
    {
        return $this->order_external_id;
    }

    /**
     * Set order_purchase_date
     *
     * @param \DateTime $orderPurchaseDate
     * @return Commande
     */
    public function setOrderPurchaseDate(\DateTime $orderPurchaseDate)
    {
        $this->order_purchase_date = $orderPurchaseDate;

        return $this;
    }

    /**
     * Get order_purchase_date
     *
     * @return \DateTime
     */
    public function getOrderPurchaseDate()
    {
        return $this->order_purchase_date;
    }

    /**
     * Set order_purchase_heure
     *
     * @param \DateTime $orderPurchaseHeure
     * @return Commande
     */
    public function setOrderPurchaseHeure(\DateTime $orderPurchaseHeure)
    {
        $this->order_purchase_heure = $orderPurchaseHeure;

        return $this;
    }

    /**
     * Get order_purchase_heure
     *
     * @return \DateTime
     */
    public function getOrderPurchaseHeure()
    {
        return $this->order_purchase_heure;
    }
}
